@extends('layouts.master')
@section('title')
Halaman Tambah VoiceActor
@endsection

@section('subtitle')
<a href="/LoveLive" class="btn btn-primary btn-sm">Back</a>
@endsection

@section('content')
<form action="/LoveLive/{{$loplep->id}}" method="POST">
  @csrf
  @method('PUT')
  <div class="form-group">
    <label>Voice Actor Name</label>
    <input type="text" name="va_name" value="{{$loplep->va_name}}" class="form-control">

  </div>
  @error('va_name')
  <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label>Character Name</label>
    <input type="text" name="chara_name" value="{{$loplep->chara_name}}" class="form-control">
  </div>
  @error('chara_name')
  <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label>Role</label>
    <input type="text" name="role" value="{{$loplep->role}}" class="form-control">
  </div>
  @error('role')
  <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label>Bio</label>
    <textarea name="bio" class="form-control" cols="30" rows="10">{{$loplep->bio}}</textarea>
  </div>
  @error('bio')
  <div class="description">{{ $message }}</div>
  @enderror
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection