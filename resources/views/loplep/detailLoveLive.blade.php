@extends('layouts.master')

@section('title')
Halaman Detail Voice Actor
@endsection

@section('subtitle')
<a href="/LoveLive" class="btn btn-primary btn-sm">Back</a>
@endsection

@section('content')
<h1>{{$loplep->va_name}}</h1>
<p>{{$loplep->bio}}</p>
@endsection