@extends('layouts.master')

@section('title')
Detail Love Live! School Idol Project
@endsection

@section('subtitle')
Details
@endsection

@section('content')
<div id="content">
    <div class="leftside" style="width: 220px">
        <img src="{{asset('assets/posterll.jpg')}}" alt="Love Live! School Idol Project" width="200px">
    </div>
    <div class="inline-block">
        <h3>Synopsis</h3>
        <p>Otonokizaka High School is in a crisis! With the number of enrolling students dropping lower and lower every year, the school is set to shut down after its current first years graduate. However, second year Honoka Kousaka refuses to let it go without a fight. Searching for a solution, she comes across popular school idol group A-RISE and sets out to create a school idol group of her own. With the help of her childhood friends Umi Sonoda and Kotori Minami, Honoka forms μ's (pronounced "muse") to boost awareness and popularity of her school.
            Unfortunately, it's all easier said than done. Student council president Eri Ayase vehemently opposes the establishment of a school idol group and will do anything in her power to prevent its creation. Moreover, Honoka and her friends have trouble attracting any additional members. But the Love Live, a competition to determine the best and most beloved school idol groups in Japan, can help them gain the attention they desperately need. With the contest fast approaching, Honoka must act quickly and diligently to try and bring together a school idol group and win the Love Live in order to save Otonokizaka High School.
        </p>
        <br>
        <h4>Background</h4>
        <p>Love Live! School Idol Project Series is a Japanese multimedia project created by Hajime Yatate and Sakurako Kimino and co-produced by Kadokawa through ASCII Media Works; Bandai Namco Music Live through music label Lantis; and animation studio Bandai Namco Filmworks (formerly known as Sunrise). Each of the individual titles within the franchise revolve around teenage girls who become "school idols". Starting in June 2010 with Love Live! School Idol Project, the franchise has seen multiple anime television series, two anime films, light novels, manga, and video games.</p>
    </div>
    <br>
    <div>
        <h5>Voice Actors and Cast
            <a href="/LoveLive/create" class="btn btn-md btn-primary float-right mr-2">Add Cast</a>
        </h5>
    </div>
    <br>

    <table class="table">
        <thead>
            <tr>
                <th scope="col">No</th>
                <th scope="col">Voice Actor Name</th>
                <th scope="col">Character Name</th>
                <th scope="col">Role</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($loplep as $key => $item)
            <tr>
                <th scope="row">{{ $key +1 }}</th>
                <td>{{$item->va_name}}</td>
                <td>{{$item->chara_name}}</td>
                <td>{{$item->role}}</td>
                <td>
                    <form action="/LoveLive/{{$item->id}}" method="post">
                        <a href="/detailLoveLive/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                        <a href="/LoveLive/{{$item->id}}/editData" class="btn btn-warning btn-sm">Edit</a>
                        @csrf
                        @method('delete')
                        <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                    </form>
                </td>
            </tr>
            @empty
            <h1>Data Kosong</h1>
            @endforelse
        </tbody>
    </table>
</div>
@endsection