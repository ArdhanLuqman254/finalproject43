@extends('layouts.master')

@section('title')
Detail K-On!
@endsection

@push('scripts')
    <script src="{{asset('/templates/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('/templates/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
    <script>
    $(function () {
        $("#example1").DataTable();
    });
    </script>
@endpush

@push('styles')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.13.2/datatables.min.css" />
@endpush

@section('content')
<div id="content">
    <div class="leftside" style="width: 220px">
        <img src="{{asset('assets/posterkon.jpg')}}" alt="Love Live! School Idol Project" width="200px">
    </div>
    <div class="inline-block">
        <h3>Synopsis</h3>
        <p>A fresh high school year always means much to come, and one of those things is joining a club. Being in a dilemma about which club to join, Yui Hirasawa stumbles upon and applies for the Light Music Club, which she misinterprets to be about playing simple instruments, such as castanets. Unable to play an instrument, she decides to visit to apologize and quit.

            Meanwhile, the Light Music Club faces disbandment due to a lack of members. This causes the club members to offer anything, from food to slacking off during club time, in order to convince Yui to join. Despite their efforts, Yui insists on leaving due to her lack of musical experience. As a last resort, they play a piece for Yui, which sparks her fiery passion and finally convinces her to join the club.
            
            From then onward, it is just plain messing around with bits and pieces of practice. The members of the Light Music Club are ready to make their time together a delightful one!
            
            [Written by MAL Rewrite]
        </p>
        <br>
        <h4>Background</h4>
        <p>The series received a Best TV Animation Award at the 2010 Tokyo International Anime Fair.</p>
    </div>
    <br>
    <div>
        <h5>Voice Actors and Cast
        <a href="/kon/create" class="btn btn-md btn-primary float-right mr-2">Add Cast</a>
        </h5>
        <br>
        <hr>
                
        <div class="table-responsive">
        <table class="table">
            <thead>
            <tr>
                <th scope="col">No</th>
                <th scope="col">Image</th>
                <th scope="col">VA Name</th>
                <th scope="col">Character Name</th>
                <th scope="col">Role</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody>
                @forelse ($kon as $key => $item)
                    
                
            <tr>
                <th scope="row">{{$kon->firstItem() + $loop->index}}</th>
                <td><img src="{{asset('/image/'.$item->image)}}" alt="{{$item->va_name}}" width="70" height="100"></td>
                <td>{{$item->va_name}}</td>
                <td>{{$item->chara_name}}</td>
                <td>{{$item->role}}</td>
                {{-- <td>{{$item->bio}}</td> --}}
                <td>
                    <form action="/kon/{{$item->id}}" method="post">
                       
                    <a href="/kon/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                    <a href="/kon/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                     @csrf
                        @method('delete')
                        <input type="submit" value="delete"class="btn btn-danger btn-sm">
                    </form>
                </td>
            </tr>
            @empty
                <h1>Data Kosong</h1>
            @endforelse
            </tbody>
        </table>
    </div>
    {{ $kon->links() }}
</div>
    
@endsection