@extends('layouts.master')

@section('title')
    Profil Voice Actor
@endsection

@section('sub-title')
    Profil
@endsection

@section('content')
<div>
   <a href="/kon" class="btn btn-primary btn-sm">Kembali</a> 
</div>


<h3>{{$kon->va_name}}</h3>
<br>
<img src="{{asset('/image/'.$kon->image)}}" alt="">
<br><br>
<p>{{$kon->bio}}</p>


@endsection