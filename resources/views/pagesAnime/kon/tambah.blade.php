@extends('layouts.master')

@section('title')
Add Cast K-On!
@endsection

@section('content')

<form action="/kon" method='post' enctype="multipart/form-data">
    @csrf
    <div class="form-group">
      <label >VA Name</label>
      <input type="text" name='va_name' class="form-control">
    </div>
    @error('va_name')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
    <label >Chara Name</label>
      <input type="text" name='chara_name' class="form-control">
    </div>
    @error('chara_name')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
    <label >Role</label>
      <select name="role" class="form-control" id="">
        <option value="">--Pilih Kategori--</option>
        <option value="Main">Main</option>
        <option value="Supporting">Supporting</option>
      </select>
    </div>
    @error('role')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
      <label>Biodata</label>
      <textarea name="bio" class="form-control" cols="30" rows="10"></textarea>
    </div>
    @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
 
    <div class="form-group">
        <label >Image</label>
        <input type="file" name='image' class="form-control">
      </div>
      @error('image')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection