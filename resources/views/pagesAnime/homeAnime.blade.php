@extends('layouts.master')

@section('title')
Daftar Anime
@endsection

@section('content')
<div class="col-8">
    <!-- <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="{{asset('assets/liella.jpg')}}" class="d-block w-100" width="100px" alt=" ...">
            </div>
            <div class="carousel-item">
                <img src="{{asset('assets/umi.jpg')}}" class="d-block w-100" alt="...">
            </div>
            <div class="carousel-item">
                <img src="{{asset('assets/rina404.jpg')}}" class="d-block w-100" alt="...">
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div> -->

    <div class="row">
        <div class="col-md-4 d-flex align-items-stretch">
            <div class="card" style="width: 15rem;">
                <img src="{{asset('assets/posterll.jpg')}}" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">Love Live! School Idol Project</h5>
                    <p class="card-text">Love Live! School Idol Project is a part of the "Love Live!" multimedia franchise, co-developed with Dengeki G's Magazine. In 2015, idol group μ's was Japan's eighth best-selling musical act, selling over eight hundred thousand CDs, DVDs, and Blu-Rays.</p>
                    <a href="/LoveLive" class="btn btn-primary">See More</a>
                </div>
            </div>
        </div>
        <div class="col-md-4 d-flex align-items-stretch">
            <div class="card" style="width: 15rem;">
                <img src="{{asset('assets/posterkon.jpg')}}" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">K-On</h5>
                    <p class="card-text">K-On is a Japanese four-panel manga series written and illustrated by Kakifly serialized in Houbunsha's Manga Time Kirara magazine between the May 2007 and October 2010 issues, and also serialized in Houbunsha's Manga Time Kirara Carat magazine.</p>
                    <a href="/kon" class="btn btn-primary">See More</a>
                </div>
            </div>
        </div>
        <div class="col-md-4 d-flex align-items-stretch">
            <div class="card" style="width: 15rem;">
                <img src="{{asset('assets/posterdrstn.jpg')}}" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">Dr Stone</h5>
                    <p class="card-text">Dr Stone is a Japanese manga series written by Riichiro Inagaki and illustrated by the South Korean artist Boichi. It was serialized in Shueisha's Weekly Shōnen Jump from March 2017 to March 2022, with its chapters collected in twenty-six tankōbon volumes.</p>
                    <a href="/DetailDrstone" class="btn btn-primary">See More</a>
                </div>
            </div>
        </div>
    </div>
    @endsection