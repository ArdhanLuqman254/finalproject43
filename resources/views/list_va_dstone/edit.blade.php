@extends('layouts.master')
@section('title')
Halaman Edit Voice Actor
    
@endsection
@section('content')
<form action="/DetailDrstone/{{$list_va_dstone->id}}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
      <label>Voice Actor Name</label>
      <input type="text" name="va_name" value="{{$list_va_dstone->va_name}}" class="form-control">

    </div>
    @error('va_name')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Character Name</label>
      <input type="text" name="chara_name" value="{{$list_va_dstone->chara_name}}" class="form-control">
    </div>
    @error('chara_name')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Role</label>
      <input type="text" name="role" value="{{$list_va_dstone->role}}" class="form-control">
    </div>
    @error('role')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Bio</label>
      <textarea name="bio" class="form-control" cols="30" rows="10">{{$list_va_dstone->bio}}</textarea>
    </div>
    @error('bio')
    <div class="description">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection