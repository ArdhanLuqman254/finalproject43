@extends('layouts.master')

@section('title')
Detail Dr. Stone

@endsection

@section('content')

<div class="leftside" style="width: 220px">
  <img src="{{asset('assets/posterdrstn.jpg')}}" alt="Love Live! School Idol Project" width="200px">
</div>
<div class="inline-block">
  <h3>Synopsis</h3>
  <p>After five years of harboring unspoken feelings, high-schooler Taiju Ooki is finally ready to confess his love to Yuzuriha Ogawa. Just when Taiju begins his confession however, a blinding green light strikes the Earth and petrifies mankind around the world—turning every single human into stone.Several millennia later, Taiju awakens to find the modern world completely nonexistent, as nature has flourished in the years humanity stood still. Among a stone world of statues, Taiju encounters one other living human: his science-loving friend Senkuu, who has been active for a few months. Taiju learns that Senkuu has developed a grand scheme—to launch the complete revival of civilization with science. Taiju's brawn and Senkuu's brains combine to forge a formidable partnership, and they soon uncover a method to revive those petrified.However, Senkuu's master plan is threatened when his ideologies are challenged by those who awaken. All the while, the reason for mankind's petrification remains unknown.
  </p>
  <br>
  <h4>Background</h4>
  <p>Dr. Stone adapts chapters 1-60 of the manga. </p>
</div>
<br>
<div>
  <h5>Voice Actors and Cast
    <a href="/DetailDrstone/create" class="btn btn-md btn-primary float-right mr-2">Add Cast</a>
  </h5>
</div>
<br>


<table class="table">
  <thead>
    <tr>
      <th scope="col">No</th>
      <th scope="col">Voice Actor Name</th>
      <th scope="col">Character Name</th>
      <th scope="col">Role</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
    @forelse ($list_va_dstone as $key => $item)
    <tr>
      <th scope="row">{{ $key +1 }}</th>
      <td>{{$item->va_name}}</td>
      <td>{{$item->chara_name}}</td>
      <td>{{$item->role}}</td>
      <td>
        <form action="/DetailDrstone/{{$item->id }}" method="post">
          <a href="/DetailDrstone/{{$item->id }}" class="btn btn-info btn-sm">Detail</a>
          <a href="/DetailDrstone/{{$item->id }}/edit" class="btn btn-warning btn-sm">Edit</a>
          @csrf
          @method('delete')
          <input type="submit" value="delete" class="btn btn-danger btn-sm">
        </form>
      </td>
    </tr>
    @empty
    <h1>Data Kosong</h1>
    @endforelse


  </tbody>
</table>
@endsection