@extends('layouts.master')

@section('title')
    Home
@endsection

@section('content')
    <h1>
        SanberBook
    </h1>
    <h2>
        Social Media Developer Santai Berkualitas
    </h2>
    <p>Belajar dan berbagi agar hidup ini semakin Santai Berkualitas</p>
    <h3>
        Benefit join di SanberBook
    </h3>
    <ul>
        <li>mendapatkan motivasi dari sesama developer</li>
        <li>Sharing Knowledge dari para mastah sanber</li>
        <li>dibuat oleh calon web developer terbaik</li>
    </ul>
    <h3>
        Cara Bergabung di SanberBook
    </h3>
    <ol>
        <li>Mengunjungi Website ini</li>
        <li>Mendaftar di<a href="/register"> Form Sign Up</a></li>
        <li>Selesai!</li>
    </ol>
@endsection