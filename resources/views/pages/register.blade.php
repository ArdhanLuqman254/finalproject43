<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="post">
        @csrf
        <label>First Name</label><br>
        <input type="text" name="fname"><br><br>
        <label>Last Name</label><br>
        <input type="text" name="lname"> <br><br>
        <label>Gender:</label><br>
        <input type="radio">Male<br>
        <input type="radio">Female<br>
        <input type="radio">Other<br><br>
        <label>Nationality:</label><br>
        <select name="Nation">
            <option value="Indo">Indonesia</option>
            <option value="US">Amerika</option>
            <option value="UK">Inggris</option>
        </select><br><br>
        <label>Language Spoken</label><br>
        <input type="checkbox" name="lang">Indonesia<br>
        <input type="checkbox" name="lang">English<br>
        <input type="checkbox" name="lang">Other<br><br>
        <label>Bio:</label><br>
        <textarea name="bio" rows="10" cols="25"></textarea><br><br>
        <input type="submit" value="submit">
    </form>
</body>

</html>