<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\homeController;
use App\Http\Controllers\authController;
use App\Http\Controllers\animeController;
use App\Http\Controllers\VoiceActorController;
use App\Http\Controllers\KonController;
use App\Http\Controllers\loplepController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route Final Project
Route::get('/', [animeController::class, 'anime']);

//Route Love Live
Route::get('/LoveLive', [loplepController::class, 'loplep']);
Route::get('/LoveLive/create', [loplepController::class, 'create']);
Route::post('/LoveLive', [loplepController::class, 'store']);

Route::get('/LoveLive', [loplepController::class, 'index']);
Route::get('/detailLoveLive/{id}', [loplepController::class, 'show']);

Route::get('/LoveLive/{id}/editData', [loplepController::class, 'edit']);
Route::put('/LoveLive/{id}', [loplepController::class, 'update']);

Route::delete('/LoveLive/{id}', [loplepController::class, 'destroy']);




//Default Route tugas
Route::get('/homes', [homeController::class, 'index']);
Route::get('/register', [authController::class, 'register']);

Route::post('/welcome', [authController::class, 'send']);

Route::get('/master', function () {
    return view('layouts.master');
});


//Route Drstone
Route::get('/DetailDrstone/create', [VoiceActorController::class, 'create']);
Route::post('/DetailDrstone', [VoiceActorController::class, 'store']);
Route::get('/DetailDrstone', [VoiceActorController::class, 'inDex']);
Route::get('/DetailDrstone/{id}', [VoiceActorController::class, 'show']);

Route::get('/DetailDrstone/{id}/edit', [VoiceActorController::class, 'edit']);
Route::put('/DetailDrstone/{id}', [VoiceActorController::class, 'update']);

Route::delete('/DetailDrstone/{id}', [VoiceActorController::class, 'destroy']);


// Route resource K-on!
Route::resource('/kon', KonController::class);
