<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LoplepController extends Controller
{
    public function loplep()
    {
        return view('loplep.tampilLoveLive');
    }

    public function create()
    {
        return view('loplep.addData');
    }

    public function store(Request $request)
    {
        $request->validate([
            'va_name' => 'required',
            'chara_name' => 'required',
            'role' => 'required',
            'bio' => 'required'
        ]);
        DB::table('loplep')->insert([
            'va_name' => $request['va_name'],
            'chara_name' => $request['chara_name'],
            'role' => $request['role'],
            'bio' => $request['bio']
        ]);
        return redirect('LoveLive');
    }

    public function index()
    {
        $loplep = DB::table('loplep')->get();

        return view('loplep.tampilLoveLive', ['loplep' => $loplep]);
    }

    public function show($id)
    {
        $loplep = DB::table('loplep')->find($id);
        return view('loplep.detailLoveLive', ['loplep' => $loplep]);
    }

    public function edit($id)
    {
        $loplep = DB::table('loplep')->find($id);
        return view('loplep.editData', ['loplep' => $loplep]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'va_name' => 'required',
            'chara_name' => 'required',
            'role' => 'required',
            'bio' => 'required'
        ]);

        DB::table('loplep')->where('id', $id)->update([
            'va_name' => $request['va_name'],
            'chara_name' => $request['chara_name'],
            'role' => $request['role'],
            'bio' => $request['bio']
        ]);
        return redirect('LoveLive');
    }

    public function destroy($id)
    {
        DB::table('loplep')->where('id','=', $id)->delete();
        return redirect('/LoveLive');
    }
}
