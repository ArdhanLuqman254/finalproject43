<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class authController extends Controller
{
    public function register()
    {
        return view('pages.register');
    }

    public function send(Request $request)
    {
        $finame = $request['fname'];
        $laname = $request['lname'];
        return view('pages.welcome', ['finame' => $finame, 'laname' => $laname]);
    }
}
