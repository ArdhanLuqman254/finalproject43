<?php

namespace App\Http\Controllers;

use App\Models\Kon;
use Illuminate\Http\Request;
use File;

class KonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kon = Kon::paginate(5);
        return view('pagesAnime.kon.detailKon',['kon' => $kon]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kon = Kon::all();
        return view('pagesAnime.kon.tambah', ['kon' => $kon]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'va_name' => 'required',
            'chara_name' => 'required',
            'role' => 'required',
            'bio' => 'required',
            'image' => 'required|mimes:png,jpg,jpeg|max:2048'
        ]);
        // Konversi nama gambar
        $imageName = time().'.'.$request->image->extension();

        // Simpan gambar ke public-image
        $request->image->move(public_path('image'), $imageName);

        // Insert ke db
        $kon = new Kon;

        $kon->va_name = $request->va_name;
        $kon->chara_name = $request->chara_name;
        $kon->role = $request->role;
        $kon->bio = $request->bio;
        $kon->image = $imageName;

        $kon->save();

        return redirect('/kon');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $kon = Kon::find($id);
        return view('pagesAnime.kon.show', ['kon' => $kon]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kon = Kon::find($id);

        return view('pagesAnime.kon.edit', ['kon' => $kon]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'va_name' => 'required',
            'chara_name' => 'required',
            'role' => 'required',
            'bio' => 'required',
            'image' => 'mimes:png,jpg,jpeg|max:2048'
        ]);

        $kon = Kon::find($id);

        $kon->va_name = $request->va_name;
        $kon->chara_name = $request->chara_name;
        $kon->role = $request->role;
        $kon->bio = $request->bio;

        if ($request->has('image')) {
            $path = 'image/';
            File::delete($path.$kon->image);

            $imageName = time().'.'.$request->image->extension();
            $request->image->move(public_path('image'), $imageName);

            $kon->image = $imageName;
        }

        $kon->save();

        return redirect('/kon');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kon = Kon::find($id);
        $path = 'image/';
        File::delete($path.$kon->image);

        $kon->delete();

        return redirect('/kon');
    }
}
