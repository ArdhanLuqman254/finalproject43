<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class VoiceActorController extends Controller
{
    public function create()
    {
        return view('list_va_dstone.tambah');
    }

    public function store(Request $request)
    {
        $request->validate([
            'va_name' => 'required',
            'chara_name' => 'required',
            'role' => 'required',
            'bio' => 'required'
        ]);
        DB::table('list_va_dstone')->insert([
            'va_name' => $request['va_name'],
            'chara_name' => $request['chara_name'],
            'role' => $request['role'],
            'bio' => $request['bio']
        ]);
        return redirect('DetailDrstone');
    }

    public function index()
    {
        $list_va_dstone = DB::table('list_va_dstone')->get();

        return view('list_va_dstone.tampil', ['list_va_dstone' => $list_va_dstone]);
    }

    public function show($id)
    {
        $list_va_dstone = DB::table('list_va_dstone')->find($id);
        return view('list_va_dstone.detail', ['list_va_dstone' => $list_va_dstone]);
    }

    public function edit($id)
    {
        $list_va_dstone = DB::table('list_va_dstone')->find($id);
        return view('list_va_dstone.edit', ['list_va_dstone' => $list_va_dstone]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'va_name' => 'required',
            'chara_name' => 'required',
            'role' => 'required',
            'bio' => 'required'
        ]);

        DB::table('list_va_dstone')
            ->where('id', $id)
            ->update(
                [
                    'va_name' => $request['va_name'],
                    'chara_name' => $request['chara_name'],
                    'role' => $request['role'],
                    'bio' => $request['bio']
                ]
            );
        return redirect('DetailDrstone');
    }

    public function destroy($id)
    {
        DB::table('list_va_dstone')->where('id', '=', $id)->delete();
        return redirect('DetailDrstone');
    }
}
