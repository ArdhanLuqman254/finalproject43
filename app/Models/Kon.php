<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kon extends Model
{
    use HasFactory;

    protected $table = 'list_va_kon';
    protected $fillable = ['va_name','chara_name','role','bio','image'];
}
