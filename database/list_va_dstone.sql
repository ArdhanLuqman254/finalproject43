-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 29, 2023 at 12:08 PM
-- Server version: 10.4.27-MariaDB
-- PHP Version: 8.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `finalproject43`
--

-- --------------------------------------------------------

--
-- Table structure for table `list_va_dstone`
--

CREATE TABLE `list_va_dstone` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `va_name` varchar(255) NOT NULL,
  `chara_name` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL,
  `bio` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `list_va_dstone`
--

INSERT INTO `list_va_dstone` (`id`, `va_name`, `chara_name`, `role`, `bio`, `created_at`, `updated_at`) VALUES
(1, 'Yuusuke Kobayashi', 'Ishigami Senku', 'Main', 'Given name: 裕介\r\nFamily name: 小林\r\nAlternate names: Yuurin-san\r\nBirthday: Mar 25, 1985\r\nWebsite:\r\nMember Favorites: 6,291\r\nMore:\r\nHe lived in Newbury, England between 5 to 10 years old, according to his appearance in \"Kaji100!: Kaji Yuuki ga Yaritai 100 no Koto.\"', NULL, NULL),
(2, 'Kengo Kawanishi', 'Asagiri Gen', 'Main', 'Given name: 健吾\r\nFamily name: 河西\r\nBirthday: Feb 18, 1985\r\nWebsite:\r\nMember Favorites: 596\r\nMore:\r\nBirthplace: Osaka Prefecture\r\nHeight: 158 cm\r\nBlood Type: A\r\nHobby / Special Skills: dance, karaoke, games', NULL, NULL),
(3, 'Gen Satou', 'Chrome', 'Main', 'Given name: 元\r\nFamily name: 佐藤\r\nBirthday: Mar 22, 1997\r\nWebsite:\r\nMember Favorites: 151\r\nMore:\r\nHometown: Kanagawa, Japan', NULL, NULL),
(4, 'Manami Numakura', 'Kohaku', 'Main', 'Given name: 愛美\r\nFamily name: 沼倉\r\nBirthday: Apr 15, 1988\r\nWebsite: http://numakuramanami.c...\r\nMember Favorites: 1,014\r\nMore:\r\nBirth place: Hiratsuka, Kanagawa, Japan\r\nBlood type: AB', NULL, NULL),
(5, 'Makoto Furukawa', 'Taiju', 'Main', 'Given name: 慎\r\nFamily name: 古川\r\nBirthday: Sep 29, 1989\r\nWebsite:\r\nMember Favorites: 4,446\r\nMore:\r\nHometown: Kumamoto, Japan\r\nBlood type: A\r\nNickname(s): Makoradon (まこらどん)', NULL, NULL),
(6, 'Karin Takahashi', 'Suika', 'Supporting', 'Given name: 花林\r\nFamily name: 高橋\r\nBirthday: Sep 9, 1994\r\nWebsite: http://ameblo.jp/cinder...\r\nMember Favorites: 169\r\nMore:\r\nBlood type: B\r\nBirth place: Kanagawa Prefecture, Japan\r\n\r\nShe is the winner of the 1st Annual Koetama Audition. Formed the unit YURI*KARI, with the 2nd Annual Koetama Audition winner, Endou Yurika.', NULL, NULL),
(7, 'Yuuichi', 'Nakamura', 'Supporting', 'Given name: 悠一\r\nFamily name: 中村\r\nAlternate names: Chuutatsu, 仲達, Yuichi Nakamura\r\nBirthday: Feb 20, 1980\r\nWebsite:\r\nMember Favorites: 30,180\r\nMore:\r\nHometown: Kagawa Prefecture, Japan\r\nHeight: 175 cm\r\nWeight: 65 kg\r\nBlood type: B\r\n\r\nChuutatsu (仲達) is the alias of Yuuichi Nakamura for visual novel/games.\r\n\r\nTrivia:\r\n- His nickname \"You-Kyan\" was named by Suzumura Kenichi.\r\n- He loves cats but is afraid of having them die before him, so he cannot have one of his own. He also he has an allergy to cat.\r\n- he is allergic to alcohol and mostly drinks soft drinks or orange juice during parties.\r\n- He has color-vision deficiency, and weak at identifying yellow.', NULL, NULL),
(8, 'Mugihito', 'Kaseki', 'Supporting', 'Given name:\r\nFamily name: 麦人\r\nAlternate names: Makoto Terada, 寺田 誠, Tadashi Oomaeda, 大前田 伝, Mugihito Tenchi, 天地 麦人\r\nBirthday: Aug 8, 1944\r\nWebsite: http://www.mugihito.com/\r\nMember Favorites: 94\r\nMore:\r\nBirth name: Makoto Terada (寺田 誠)\r\nHometown: Musashino, Tokyo, Japan\r\nBlood type: A', NULL, NULL),
(9, 'Kana Ichinose', 'Yuzuriha', 'Supporting', 'Given name: 加那\r\nFamily name: 市ノ瀬\r\nBirthday: Dec 20, 1996\r\nWebsite:\r\nMember Favorites: 1,256\r\nMore:\r\nHometown: Hokkaido\r\nHeight: 148 cm\r\nHobbies: Traveling alone, sky watching, watching movies\r\nSkills & Abilities: Basketball', NULL, NULL),
(10, 'Keiji Fujiwara', 'Byakuya', 'Supporting', 'Given name: 啓治\r\nFamily name: 藤原\r\nAlternate names: 子太明\r\nBirthday: Oct 5, 1964\r\nWebsite: http://www.air-agency.c...\r\nMember Favorites: 4,411\r\nMore:\r\nBlood type: A\r\nBirth place: Tokyo, Japan\r\nDate of death: April 12, 2020\r\n\r\nFujiwara was born in Tokyo. He spent the majority of his childhood in Iwate Prefecture. In high school, he handled vocals in a band he formed with his friend Kotaro Furuichi, future guitarist of rock band The Collectors.\r\n\r\nAround 18, he moved back to Tokyo on his own and joined the Bungakuza acting school. He spent the 1980s performing in several theater troupes while working odd jobs. Fujiwara was introduced to his first voice acting agency, Ken Production, in the early 1990s. The first TV anime in which he appeared as a regular was Yokoyama Mitsuteru Sangokushi, but his breakout role was Hiroshi Nohara in Crayon Shin-chan.\r\n\r\nIn November 2006, he left Ken Production and founded his own agency, AIR AGENCY Co., Ltd, of which he also served as the Representative Director of the company. Aside from talent management, the company went on to release original drama and situation CDs through its child company Air Label, as well as produce live events. in 2010, Fujiwara made his own sound directorial debut in Kakkokawaii Sengen!\r\n\r\nFujiwara was a regular lecturer at the Japan Newart College since 2008.\r\n\r\nIn August 2016, Air Agency announced that he was going on hiatus in order to undergo medical treatment for a then-unspecified illness. He officially resumed work in June 2017. Fujiwara died of cancer on April 12, 2020.', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `list_va_dstone`
--
ALTER TABLE `list_va_dstone`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `list_va_dstone`
--
ALTER TABLE `list_va_dstone`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
