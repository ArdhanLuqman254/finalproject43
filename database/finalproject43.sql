-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3307
-- Generation Time: Mar 31, 2023 at 03:26 PM
-- Server version: 10.4.27-MariaDB
-- PHP Version: 8.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `finalproject43`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) NOT NULL,
  `connection` text NOT NULL,
  `queue` text NOT NULL,
  `payload` longtext NOT NULL,
  `exception` longtext NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `list_va_dstone`
--

CREATE TABLE `list_va_dstone` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `va_name` varchar(255) NOT NULL,
  `chara_name` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL,
  `bio` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `list_va_dstone`
--

INSERT INTO `list_va_dstone` (`id`, `va_name`, `chara_name`, `role`, `bio`, `created_at`, `updated_at`) VALUES
(1, 'Yuusuke Kobayashi', 'Ishigami Senku', 'Main', 'Given name: 裕介\r\nFamily name: 小林\r\nAlternate names: Yuurin-san\r\nBirthday: Mar 25, 1985\r\nWebsite:\r\nMember Favorites: 6,291\r\nMore:\r\nHe lived in Newbury, England between 5 to 10 years old, according to his appearance in \"Kaji100!: Kaji Yuuki ga Yaritai 100 no Koto.\"', NULL, NULL),
(2, 'Kengo Kawanishi', 'Asagiri Gen', 'Main', 'Given name: 健吾\r\nFamily name: 河西\r\nBirthday: Feb 18, 1985\r\nWebsite:\r\nMember Favorites: 596\r\nMore:\r\nBirthplace: Osaka Prefecture\r\nHeight: 158 cm\r\nBlood Type: A\r\nHobby / Special Skills: dance, karaoke, games', NULL, NULL),
(3, 'Gen Satou', 'Chrome', 'Main', 'Given name: 元\r\nFamily name: 佐藤\r\nBirthday: Mar 22, 1997\r\nWebsite:\r\nMember Favorites: 151\r\nMore:\r\nHometown: Kanagawa, Japan', NULL, NULL),
(4, 'Manami Numakura', 'Kohaku', 'Main', 'Given name: 愛美\r\nFamily name: 沼倉\r\nBirthday: Apr 15, 1988\r\nWebsite: http://numakuramanami.c...\r\nMember Favorites: 1,014\r\nMore:\r\nBirth place: Hiratsuka, Kanagawa, Japan\r\nBlood type: AB', NULL, NULL),
(5, 'Makoto Furukawa', 'Taiju', 'Main', 'Given name: 慎\r\nFamily name: 古川\r\nBirthday: Sep 29, 1989\r\nWebsite:\r\nMember Favorites: 4,446\r\nMore:\r\nHometown: Kumamoto, Japan\r\nBlood type: A\r\nNickname(s): Makoradon (まこらどん)', NULL, NULL),
(6, 'Karin Takahashi', 'Suika', 'Supporting', 'Given name: 花林\r\nFamily name: 高橋\r\nBirthday: Sep 9, 1994\r\nWebsite: http://ameblo.jp/cinder...\r\nMember Favorites: 169\r\nMore:\r\nBlood type: B\r\nBirth place: Kanagawa Prefecture, Japan\r\n\r\nShe is the winner of the 1st Annual Koetama Audition. Formed the unit YURI*KARI, with the 2nd Annual Koetama Audition winner, Endou Yurika.', NULL, NULL),
(7, 'Yuuichi', 'Nakamura', 'Supporting', 'Given name: 悠一\r\nFamily name: 中村\r\nAlternate names: Chuutatsu, 仲達, Yuichi Nakamura\r\nBirthday: Feb 20, 1980\r\nWebsite:\r\nMember Favorites: 30,180\r\nMore:\r\nHometown: Kagawa Prefecture, Japan\r\nHeight: 175 cm\r\nWeight: 65 kg\r\nBlood type: B\r\n\r\nChuutatsu (仲達) is the alias of Yuuichi Nakamura for visual novel/games.\r\n\r\nTrivia:\r\n- His nickname \"You-Kyan\" was named by Suzumura Kenichi.\r\n- He loves cats but is afraid of having them die before him, so he cannot have one of his own. He also he has an allergy to cat.\r\n- he is allergic to alcohol and mostly drinks soft drinks or orange juice during parties.\r\n- He has color-vision deficiency, and weak at identifying yellow.', NULL, NULL),
(8, 'Mugihito', 'Kaseki', 'Supporting', 'Given name:\r\nFamily name: 麦人\r\nAlternate names: Makoto Terada, 寺田 誠, Tadashi Oomaeda, 大前田 伝, Mugihito Tenchi, 天地 麦人\r\nBirthday: Aug 8, 1944\r\nWebsite: http://www.mugihito.com/\r\nMember Favorites: 94\r\nMore:\r\nBirth name: Makoto Terada (寺田 誠)\r\nHometown: Musashino, Tokyo, Japan\r\nBlood type: A', NULL, NULL),
(9, 'Kana Ichinose', 'Yuzuriha', 'Supporting', 'Given name: 加那\r\nFamily name: 市ノ瀬\r\nBirthday: Dec 20, 1996\r\nWebsite:\r\nMember Favorites: 1,256\r\nMore:\r\nHometown: Hokkaido\r\nHeight: 148 cm\r\nHobbies: Traveling alone, sky watching, watching movies\r\nSkills & Abilities: Basketball', NULL, NULL),
(10, 'Keiji Fujiwara', 'Byakuya', 'Supporting', 'Given name: 啓治\r\nFamily name: 藤原\r\nAlternate names: 子太明\r\nBirthday: Oct 5, 1964\r\nWebsite: http://www.air-agency.c...\r\nMember Favorites: 4,411\r\nMore:\r\nBlood type: A\r\nBirth place: Tokyo, Japan\r\nDate of death: April 12, 2020\r\n\r\nFujiwara was born in Tokyo. He spent the majority of his childhood in Iwate Prefecture. In high school, he handled vocals in a band he formed with his friend Kotaro Furuichi, future guitarist of rock band The Collectors.\r\n\r\nAround 18, he moved back to Tokyo on his own and joined the Bungakuza acting school. He spent the 1980s performing in several theater troupes while working odd jobs. Fujiwara was introduced to his first voice acting agency, Ken Production, in the early 1990s. The first TV anime in which he appeared as a regular was Yokoyama Mitsuteru Sangokushi, but his breakout role was Hiroshi Nohara in Crayon Shin-chan.\r\n\r\nIn November 2006, he left Ken Production and founded his own agency, AIR AGENCY Co., Ltd, of which he also served as the Representative Director of the company. Aside from talent management, the company went on to release original drama and situation CDs through its child company Air Label, as well as produce live events. in 2010, Fujiwara made his own sound directorial debut in Kakkokawaii Sengen!\r\n\r\nFujiwara was a regular lecturer at the Japan Newart College since 2008.\r\n\r\nIn August 2016, Air Agency announced that he was going on hiatus in order to undergo medical treatment for a then-unspecified illness. He officially resumed work in June 2017. Fujiwara died of cancer on April 12, 2020.', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `list_va_kon`
--

CREATE TABLE `list_va_kon` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `va_name` varchar(255) NOT NULL,
  `chara_name` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL,
  `bio` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `list_va_kon`
--

INSERT INTO `list_va_kon` (`id`, `va_name`, `chara_name`, `role`, `bio`, `image`, `created_at`, `updated_at`) VALUES
(2, 'Valenzuela, Cristina', 'Akiyama, Mio', 'Main', 'Alternate names: Cristina Danielle Valenzuela, Cristina Vee, Cristina Vee Valenzuela\r\nBirthday: Jul 11, 1987\r\nWebsite: http://cristinavee.com/\r\nMember Favorites: 1,479\r\nMore:\r\nIn addition to her anime resume, she also voices a lot of video game characters and some live action dubs.\r\n\r\nTwitter: @CristinaVee\r\nInstagram: @cristinavox', '1680093991.jpg', '2023-03-29 05:46:31', '2023-03-29 05:46:31'),
(3, 'Hikasa, Youko', 'Akiyama, Mio', 'Main', 'Given name: 陽子\r\nFamily name: 日笠\r\nAlternate names: Yoko Hikasa\r\nBirthday: Jul 16, 1985\r\nWebsite: https://lineblog.me/hik...\r\nMember Favorites: 12,724\r\nMore:\r\nHometown: Kanagawa, Japan\r\nBlood type: O\r\nHeight: 157 cm\r\nSports and Hobbies: fashion, basketball, softball\r\n\r\nCo-hosts the popular weekly radio show Odoroki Sentai Momonoki Five (おどろき戦隊モモノキファイブ) with Nakamura Eriko.\r\n\r\nMarried in 2015.\r\nLeft I\'m Enterprise and became Freelance on January 20, 2023.\r\n\r\nInstagram: @hikasayoko_official', '1680094072.jpg', '2023-03-29 05:47:52', '2023-03-29 05:47:52'),
(5, 'Jeong, Mi Sook', 'Hirasawa, Yui', 'Main', 'Given name: 미숙\r\nFamily name: 정\r\nBirthday: Dec 25, 1962\r\nWebsite: http://cafe.daum.net/misukangel\r\nMember Favorites: 33\r\nMore:\r\nMi Sook Jeong joined KBS Voice Acting Division in 1984.', '1680095894.jpg', '2023-03-29 06:18:14', '2023-03-29 06:18:14'),
(6, 'Kotobuki, Minako', 'Kotobuki, Tsumugi', 'Main', 'Given name: 美菜子\r\nFamily name: 寿\r\nAlternate names: Sphere, スフィア\r\nBirthday: Sep 17, 1991\r\nWebsite: http://www.kotobukimina...\r\nMember Favorites: 1,659\r\nMore:\r\nBirth place: Nagata Ward, Kobe City, Hyogo Prefecture, Japan\r\nBlood type: B\r\nHeight: 158 cm\r\nFavorite animal: Koala, ferrets\r\nFavorite beverage: Orange juice, cocoa\r\nFavorite food: All noodles, squash\r\nFavorite saying: Strong convictions and a bright smile have the power to make miracles.\r\nFavorite sport: Basketball\r\nFavorite subject: Physical education and Japanese language\r\nHobbies: Dancing, Listening to Music, Reading Books, Watching Movies\r\n\r\nOne of the members of seiyuu group, Sphere.\r\n\r\nBlog: ameblo.jp/kotobukiminako', '1680190052.jpg', '2023-03-30 08:27:32', '2023-03-30 08:27:32'),
(7, 'Cabanos, Christine Marie', 'Nakano, Azusa', 'Main', 'Birthday: Jul 12, 1988\r\nWebsite: http://www.christinemar...\r\nMember Favorites: 713\r\nMore:\r\nAwards:\r\n- Best Female Lead Vocal Performance in an Anime Movie/Special at 4th Annual BTVA Anime Dub Awards (People\'s Choice)\r\n\r\nTwitter: @christinemcabz', '1680190113.jpg', '2023-03-30 08:28:33', '2023-03-30 08:28:33'),
(8, 'Morris, Cassandra Lee', 'Tainaka, Ritsu', 'Main', 'Alternate names: Meghan McCracken, Chloe Ragnbone\r\nBirthday: Apr 19, 1982\r\nWebsite: http://www.cassandralee...\r\nMember Favorites: 797\r\nMore:\r\nFacebook: @ilovecassandralee\r\nTwitter: @SoCassandra\r\nIG: @cassandra.morris\r\nYoutube: @btwixter', '1680190208.jpg', '2023-03-30 08:30:08', '2023-03-30 08:30:08'),
(9, 'Kitazawa, Riki', 'Hirasawa, Father', 'Supporting', 'Given name: 力\r\nFamily name: 北沢\r\nAlternate names: 北澤 力\r\nBirthday: Jul 26\r\nWebsite: https://haikyo.co.jp/profile/profile.php?ActorID=12111\r\nMember Favorites: 3\r\nMore:\r\nBirth place: Kanagawa Prefecture, Japan\r\nDetails News Pictures\r\nTop  >  People  >  Kitazawa, Riki\r\nAdd Voice Actor RoleVoice Acting Roles\r\nSorted by Most Recent', '1680190398.jpg', '2023-03-30 08:33:18', '2023-03-30 08:33:18'),
(10, 'Natsuki, Rio', 'Hirasawa, Mother', 'Supporting', 'Given name: リオ\r\nFamily name: 夏樹\r\nBirthday: Mar 5, 1969\r\nWebsite: http://haikyo.co.jp/profile/profile.php?ActorID=11517\r\nMember Favorites: 13\r\nMore:\r\nBorn in Toyko, Japan.\r\nBlood Type A', '1680190467.jpg', '2023-03-30 08:34:27', '2023-03-30 08:34:27');

-- --------------------------------------------------------

--
-- Table structure for table `loplep`
--

CREATE TABLE `loplep` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `va_name` varchar(255) NOT NULL,
  `chara_name` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL,
  `bio` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `loplep`
--

INSERT INTO `loplep` (`id`, `va_name`, `chara_name`, `role`, `bio`, `created_at`, `updated_at`) VALUES
(1, 'Nitta Emi', 'Kousaka Honoka', 'Main', 'Nitta Emi, born December 10, 1985) is a Japanese voice actress and singer from Nagano Prefecture affiliated with the talent agency Difference.[1] She became a voice actress after passing an audition organized by talent agency S-inc. She officially transferred to Difference on 1 September 2017. Some of her major roles are Honoka Kōsaka in Love Live!', NULL, NULL),
(3, 'Mimori Suzuko', 'Sonoda Umi', 'Main', 'Mimori Suzuko, born June 28, 1986) is a Japanese actress and singer.[1] She is represented by HiBiKi Cast.[2] She voiced Umi Sonoda in Love Live!', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2023_03_29_054915_create_list_va_kon_table', 1),
(6, '2023_03_28_171240_create_list_va_dstone_table', 2),
(7, '2023_03_29_170113_create_loplep_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `token` varchar(64) NOT NULL,
  `abilities` text DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `list_va_dstone`
--
ALTER TABLE `list_va_dstone`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `list_va_kon`
--
ALTER TABLE `list_va_kon`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `loplep`
--
ALTER TABLE `loplep`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD PRIMARY KEY (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `list_va_dstone`
--
ALTER TABLE `list_va_dstone`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `list_va_kon`
--
ALTER TABLE `list_va_kon`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `loplep`
--
ALTER TABLE `loplep`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
