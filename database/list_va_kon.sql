-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3307
-- Generation Time: Mar 31, 2023 at 02:01 PM
-- Server version: 10.4.27-MariaDB
-- PHP Version: 8.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `finalproject43`
--

-- --------------------------------------------------------

--
-- Table structure for table `list_va_kon`
--

CREATE TABLE `list_va_kon` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `va_name` varchar(255) NOT NULL,
  `chara_name` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL,
  `bio` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `list_va_kon`
--

INSERT INTO `list_va_kon` (`id`, `va_name`, `chara_name`, `role`, `bio`, `image`, `created_at`, `updated_at`) VALUES
(2, 'Valenzuela, Cristina', 'Akiyama, Mio', 'Main', 'Alternate names: Cristina Danielle Valenzuela, Cristina Vee, Cristina Vee Valenzuela\r\nBirthday: Jul 11, 1987\r\nWebsite: http://cristinavee.com/\r\nMember Favorites: 1,479\r\nMore:\r\nIn addition to her anime resume, she also voices a lot of video game characters and some live action dubs.\r\n\r\nTwitter: @CristinaVee\r\nInstagram: @cristinavox', '1680093991.jpg', '2023-03-29 05:46:31', '2023-03-29 05:46:31'),
(3, 'Hikasa, Youko', 'Akiyama, Mio', 'Main', 'Given name: 陽子\r\nFamily name: 日笠\r\nAlternate names: Yoko Hikasa\r\nBirthday: Jul 16, 1985\r\nWebsite: https://lineblog.me/hik...\r\nMember Favorites: 12,724\r\nMore:\r\nHometown: Kanagawa, Japan\r\nBlood type: O\r\nHeight: 157 cm\r\nSports and Hobbies: fashion, basketball, softball\r\n\r\nCo-hosts the popular weekly radio show Odoroki Sentai Momonoki Five (おどろき戦隊モモノキファイブ) with Nakamura Eriko.\r\n\r\nMarried in 2015.\r\nLeft I\'m Enterprise and became Freelance on January 20, 2023.\r\n\r\nInstagram: @hikasayoko_official', '1680094072.jpg', '2023-03-29 05:47:52', '2023-03-29 05:47:52'),
(5, 'Jeong, Mi Sook', 'Hirasawa, Yui', 'Main', 'Given name: 미숙\r\nFamily name: 정\r\nBirthday: Dec 25, 1962\r\nWebsite: http://cafe.daum.net/misukangel\r\nMember Favorites: 33\r\nMore:\r\nMi Sook Jeong joined KBS Voice Acting Division in 1984.', '1680095894.jpg', '2023-03-29 06:18:14', '2023-03-29 06:18:14'),
(6, 'Kotobuki, Minako', 'Kotobuki, Tsumugi', 'Main', 'Given name: 美菜子\r\nFamily name: 寿\r\nAlternate names: Sphere, スフィア\r\nBirthday: Sep 17, 1991\r\nWebsite: http://www.kotobukimina...\r\nMember Favorites: 1,659\r\nMore:\r\nBirth place: Nagata Ward, Kobe City, Hyogo Prefecture, Japan\r\nBlood type: B\r\nHeight: 158 cm\r\nFavorite animal: Koala, ferrets\r\nFavorite beverage: Orange juice, cocoa\r\nFavorite food: All noodles, squash\r\nFavorite saying: Strong convictions and a bright smile have the power to make miracles.\r\nFavorite sport: Basketball\r\nFavorite subject: Physical education and Japanese language\r\nHobbies: Dancing, Listening to Music, Reading Books, Watching Movies\r\n\r\nOne of the members of seiyuu group, Sphere.\r\n\r\nBlog: ameblo.jp/kotobukiminako', '1680190052.jpg', '2023-03-30 08:27:32', '2023-03-30 08:27:32'),
(7, 'Cabanos, Christine Marie', 'Nakano, Azusa', 'Main', 'Birthday: Jul 12, 1988\r\nWebsite: http://www.christinemar...\r\nMember Favorites: 713\r\nMore:\r\nAwards:\r\n- Best Female Lead Vocal Performance in an Anime Movie/Special at 4th Annual BTVA Anime Dub Awards (People\'s Choice)\r\n\r\nTwitter: @christinemcabz', '1680190113.jpg', '2023-03-30 08:28:33', '2023-03-30 08:28:33'),
(8, 'Morris, Cassandra Lee', 'Tainaka, Ritsu', 'Main', 'Alternate names: Meghan McCracken, Chloe Ragnbone\r\nBirthday: Apr 19, 1982\r\nWebsite: http://www.cassandralee...\r\nMember Favorites: 797\r\nMore:\r\nFacebook: @ilovecassandralee\r\nTwitter: @SoCassandra\r\nIG: @cassandra.morris\r\nYoutube: @btwixter', '1680190208.jpg', '2023-03-30 08:30:08', '2023-03-30 08:30:08'),
(9, 'Kitazawa, Riki', 'Hirasawa, Father', 'Supporting', 'Given name: 力\r\nFamily name: 北沢\r\nAlternate names: 北澤 力\r\nBirthday: Jul 26\r\nWebsite: https://haikyo.co.jp/profile/profile.php?ActorID=12111\r\nMember Favorites: 3\r\nMore:\r\nBirth place: Kanagawa Prefecture, Japan\r\nDetails News Pictures\r\nTop  >  People  >  Kitazawa, Riki\r\nAdd Voice Actor RoleVoice Acting Roles\r\nSorted by Most Recent', '1680190398.jpg', '2023-03-30 08:33:18', '2023-03-30 08:33:18'),
(10, 'Natsuki, Rio', 'Hirasawa, Mother', 'Supporting', 'Given name: リオ\r\nFamily name: 夏樹\r\nBirthday: Mar 5, 1969\r\nWebsite: http://haikyo.co.jp/profile/profile.php?ActorID=11517\r\nMember Favorites: 13\r\nMore:\r\nBorn in Toyko, Japan.\r\nBlood Type A', '1680190467.jpg', '2023-03-30 08:34:27', '2023-03-30 08:34:27');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `list_va_kon`
--
ALTER TABLE `list_va_kon`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `list_va_kon`
--
ALTER TABLE `list_va_kon`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
