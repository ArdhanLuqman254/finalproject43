-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 31, 2023 at 02:32 PM
-- Server version: 10.4.27-MariaDB
-- PHP Version: 8.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `finalprj43`
--

-- --------------------------------------------------------

--
-- Table structure for table `loplep`
--

CREATE TABLE `loplep` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `va_name` varchar(255) NOT NULL,
  `chara_name` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL,
  `bio` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `loplep`
--

INSERT INTO `loplep` (`id`, `va_name`, `chara_name`, `role`, `bio`, `created_at`, `updated_at`) VALUES
(1, 'Nitta Emi', 'Kousaka Honoka', 'Main', 'Nitta Emi, born December 10, 1985) is a Japanese voice actress and singer from Nagano Prefecture affiliated with the talent agency Difference.[1] She became a voice actress after passing an audition organized by talent agency S-inc. She officially transferred to Difference on 1 September 2017. Some of her major roles are Honoka Kōsaka in Love Live!', NULL, NULL),
(3, 'Mimori Suzuko', 'Sonoda Umi', 'Main', 'Mimori Suzuko, born June 28, 1986) is a Japanese actress and singer.[1] She is represented by HiBiKi Cast.[2] She voiced Umi Sonoda in Love Live!', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `loplep`
--
ALTER TABLE `loplep`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `loplep`
--
ALTER TABLE `loplep`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
